import json
from operator import itemgetter

import pygame as pg
from pygame import freetype
from pygame.locals import *

# Screen Constant Variables
BOX_DIM = 32
X_DISPLAY, Y_DISPLAY = 320, 480
BG_COLOR = (150, 150, 200)

# Constant Starting Variables
X_COORD, Y_COORD = 0, 0
X_MOVE_COORD, Y_MOVE_COORD = 32, 10
DROP_TIME = 500

# Time Variables
currentTick = 0
lastTick = 0
timeSinceLastTick = 0

# Rows and Columns
column = 0
row = 0

pg.init()
BG_COLOR = pg.Color('gray12')
BLUE = pg.Color('dodgerblue')
FONT = freetype.Font(None, 24)


def save(highscores):
    with open('highscores.json', 'w') as file:
        json.dump(highscores, file)  # Write the list to the json file.


def load():
    try:
        with open('highscores.json', 'r') as file:
            highscores = json.load(file)  # Read the json file.
    except FileNotFoundError:
        return []  # Return an empty list if the file doesn't exist.
    # Sorted by the score.
    return sorted(highscores, key=itemgetter(1), reverse=True)


def main():
    screen = pg.display.set_mode((640, 480))
    clock = pg.time.Clock()
    highscores = load()  # Load the json file.

    while True:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                return
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_s:
                    # Save the sorted the list when 's' is pressed.
                    # Append a new high-score (omitted in this example).
                    # highscores.append([name, score])
                    save(sorted(highscores, key=itemgetter(1), reverse=True))

        screen.fill((30, 30, 50))
        # Display the high-scores.
        for y, (hi_name, hi_score) in enumerate(highscores):
            FONT.render_to(screen, (100, y*30+40), f'{hi_name} {hi_score}', BLUE)

        pg.display.flip()
        clock.tick(60)


def draw_block():
    surface.fill(BG_COLOR)
    surface.blit(block, (X_COORD, Y_COORD))
    pg.display.flip()


def first_three(str):
	return str[:3] if len(str) > 3 else str

def get_high_score():
    # Default high score
    high_score = 0

    # Try to read the high score from a file
    try:
        high_score_file = open("high_score.txt", "r")
        high_score = int(high_score_file.read())
        high_score_file.close()
        print("The high score is", high_score)
    except IOError:
        # Error reading file, no high score
        print("There is no high score yet.")
    except ValueError:
        # There's a file there, but we don't understand the number.
        print("I'm confused. Starting with no high score.")

    return high_score


def save_high_score(new_high_score):
    try:
        # Write the file to disk
        high_score_file = open("high_score.txt", "w")
        high_score_file.write(str(new_high_score))
        high_score_file.close()
    except IOError:
        # Hm, can't write it.
        print("Unable to save the high score.")


def score():
    """ Main program is here. """
    # Get the high score
    high_score = get_high_score()

    # Get the score from the current game
    current_score = 0
    player_name = ""
    try:
        # Ask the user for his/her score
        player_name = input("What is your name? (Enter at most three character's) ")
        player_name = input("Your name is " + first_three(player_name) + ". Press Enter!")
        current_score = int(input("What is your score? "))
    except ValueError:
        # Error, can't turn what they typed into a number
        print("I don't understand what you typed.")

    # See if we have a new high score
    if current_score > high_score:
        # We do! Save to disk
        print("Yea! New high score!")
        save_high_score(current_score)
    else:
        print("Better luck next time.")


# Call the main function, start up the game
if __name__ == "__main__":
    score()
    pg.init()

    surface = pg.display.set_mode((X_DISPLAY, Y_DISPLAY))
    surface.fill((100, 100, 255))

    block = pg.image.load("resources/egg.png").convert()

    surface.blit(block, (X_COORD, Y_COORD))

    pg.display.flip()

    running = True

    while running:
        # Detect the amount of time since the last time the block has dropped
        lastTick = currentTick
        currentTick = pg.time.get_ticks()
        timeSinceLastTick += (currentTick - lastTick)

        # Detecting and processing button presses
        for event in pg.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_LEFT and X_COORD > 0:
                    X_COORD -= X_MOVE_COORD
                    draw_block()
                if event.key == K_RIGHT and X_COORD < X_DISPLAY - BOX_DIM:  # Needed to stay on the CPR screen
                    X_COORD += X_MOVE_COORD
                    draw_block()
                if event.key == K_DOWN and Y_COORD < Y_DISPLAY:
                    Y_COORD = Y_DISPLAY - BOX_DIM
                    draw_block()

        if timeSinceLastTick >= DROP_TIME and Y_COORD < Y_DISPLAY - BOX_DIM:  # Needed to stay on the CPR screen
            timeSinceLastTick = 0
            Y_COORD += Y_MOVE_COORD
            draw_block()
