from gettext import npgettext

import pygame
from pygame import freetype
from pygame.locals import *

import random

import json
from operator import itemgetter

#Initializing
pygame.init()
running = True
alive = True

#Game Grid (0 = Nothing, 1 = Egg, 2 = Pancake, 3 = Waffle, 4 = Plate)
gridHeight, gridWidth = 16, 6
grid = [[0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0], 
        [4,4,4,4,4,4],
       ]

#Screen Constants
X_DISPLAY, Y_DISPLAY = 320, 480
BG_COLOR = (100, 100, 255)
color1 = (50, 50, 150)
color2 = (255, 255, 0)
color3 = (150, 150, 0)

egg = "resources/egg.png"
waffle = "resources/waffle.png"
pancake = "resources/pancake.png"
plate = "resources/plate.png"

#Time Variables
currentTick = 0
lastTick = 0
timeSinceLastTick = 0
numDrops = 0
newBlocksNum = 0
difficulty = 0

#Scoring
playerScore = 0
highScore = 0

DROP_TIME = 375
DROP_TIME_FAST = 100
dropFast = False
hasDropped = 0

#Score Meathods
def save(highscores):
    with open('highscores.json', 'w') as file:
        json.dump(highscores, file)  # Write the list to the json file.

def load():
    try:
        with open('highscores.json', 'r') as file:
            highscores = json.load(file)  # Read the json file.
    except FileNotFoundError:
        return []  # Return an empty list if the file doesn't exist.
    # Sorted by the score.
    return sorted(highscores, key=itemgetter(1), reverse=True)

def first_three(str):
	return str[:3] if len(str) > 3 else str

def get_high_score():
    # Default high score
    high_score = 0

    # Try to read the high score from a file
    try:
        high_score_file = open("high_score.txt", "r")
        high_score = int(high_score_file.read())
        high_score_file.close()
        print("The high score is", high_score)
    except IOError:
        # Error reading file, no high score
        print("There is no high score yet.")
    except ValueError:
        # There's a file there, but we don't understand the number.
        print("I'm confused. Starting with no high score.")

    return high_score

def save_high_score(new_high_score):
    try:
        # Write the file to disk
        high_score_file = open("high_score.txt", "w")
        high_score_file.write(str(new_high_score))
        high_score_file.close()
    except IOError:
        # Hm, can't write it.
        print("Unable to save the high score.")

def score(current_score):
    # Get the high score
    high_score = get_high_score()

    # Get the score from the current game
    current_score = 0
    player_name = ""
    try:
        # Ask the user for his/her score
        player_name = input("What is your name? (Enter at most three character's) ")
        player_name = input("Your name is " + first_three(player_name) + ". Press Enter!")
        current_score = int(input("What is your score? "))
    except ValueError:
        # Error, can't turn what they typed into a number
        print("I don't understand what you typed.")

    # See if we have a new high score
    if current_score > high_score:
        # We do! Save to disk
        print("Yea! New high score!")
        save_high_score(current_score)
    else:
        print("Better luck next time.")


def updateMenu():
        surface.fill(BG_COLOR)
        colorbg = (50, 50, 150)
        coloryellow = (255, 255, 0)
        colorteal = (0, 255, 255)
        colordarkyellow = (150, 150, 0)

        Font = pygame.font.SysFont('commando', 70)
        Font1 = pygame.font.SysFont('commando', 70)
        Font2 = pygame.font.SysFont('commando', 20)
        letterbreaktris1 = Font1.render("BREAKTRIS", False, colordarkyellow)
        letterbreaktris2 = Font.render("BREAKTRIS", False, coloryellow)
        letterpressanybutton1 = Font2.render("PRESS ANY BUTTON TO START", False, colordarkyellow)
        letterpressanybutton2 = Font2.render("PRESS ANY BUTTON TO START", False, coloryellow)

        surface.blit(letterbreaktris1, (8, 69)) #nice
        surface.blit(letterbreaktris2, (11, 72))
        surface.blit(letterpressanybutton1, (59, 389))
        surface.blit(letterpressanybutton2, (60, 390))
        surface.blit(pygame.image.load(egg).convert(), (95, 200))
        surface.blit(pygame.image.load(pancake).convert(), (145, 200))
        surface.blit(pygame.image.load(waffle).convert(), (195, 200))

#Draws the UI when called
def updateUI():
    pygame.draw.rect(surface, color1, pygame.Rect(0, 0, 320, 40))
    pygame.draw.rect(surface, color1, pygame.Rect(0, 0, 108, 120))
    pygame.draw.rect(surface, color1, pygame.Rect(212, 0, 320, 120))
    pygame.draw.rect(surface, color2, pygame.Rect(0, 170, 320, 2))

    Font = pygame.font.SysFont('commando', 30)
    Font2 = pygame.font.SysFont('commando', 30)
    letter1 = Font.render("SCORE " + "{:04d}".format(playerScore), False, color2)
    letter2 = Font.render("SET 000", False, color2)
    letter3 = Font.render("NEXT ->", False, color2)
    letter4 = Font.render("LEVEL", False, color2)
    letter5 = Font.render("{:03d}".format(difficulty + 1), False, color2)
    letter6 = Font.render("HIGH " + "{:04d}".format(highScore), False, color2)
    letter11 = Font.render("SCORE " + "{:04d}".format(playerScore), False, color3)
    letter21 = Font.render("SET 000", False, color3)
    letter31 = Font.render("NEXT ->", False, color3)
    letter41 = Font.render("LEVEL", False, color3)
    letter51 = Font.render("{:03d}".format(difficulty + 1), False, color3)
    

    surface.blit(letter11, (9, 11))
    surface.blit(letter21, (11, 45))
    surface.blit(letter31, (11, 79))
    surface.blit(letter41, (237, 45))
    surface.blit(letter51, (251, 79))
    surface.blit(letter1, (10, 12))
    surface.blit(letter2, (12, 46))
    surface.blit(letter3, (12, 80))
    surface.blit(letter4, (238, 46))
    surface.blit(letter5, (252, 80))
    surface.blit(letter6, (200, 10))

#Returns a number 1-3
def getRandomNum():
    return random.randrange(1,4)

#Draws the grid to the screen in order from bottom to top
def drawBlocks():
    i = gridHeight - 1
    while i >= 0:
        j = 0
        while j < gridWidth:
            if grid[i][j] == 1:
                surface.blit(pygame.image.load(egg).convert(),(5 + ((j + 1) * 32) + ((j - 1) * 18),150 + ((i * 20))))
            if grid[i][j] == 2:
                surface.blit(pygame.image.load(pancake).convert(),(5 + ((j + 1) * 32) + ((j - 1) * 18),150 + ((i * 20))))
            if grid[i][j] == 3:
                surface.blit(pygame.image.load(waffle).convert(),(5 + ((j + 1) * 32) + ((j - 1) * 18),150 + ((i * 20))))
            if grid[i][j] == 4:
                surface.blit(pygame.image.load(plate).convert(),(5 + ((j + 1) * 32) + ((j - 1) * 18),138 + ((i * 20))))
            j += 1
        i -= 1

#Drops all blocks on the screen
def dropBlocks():
    i = gridHeight - 1 
    while i >= 1:
        j = 0
        while j < gridWidth:
            if grid[i][j] == 0 and grid[i - 1][j] != 0:
                grid[i][j] = grid[i - 1][j]
                grid[i - 1][j] = 0
            j += 1
        i -= 1

#Moves both the blocks to the left
def moveLeft():
    i = 0
    while i < gridHeight:
        j = 0
        while j < gridWidth:
            if (j != 0) and (grid[i][j] == 1 or grid[i][j] == 2 or grid[i][j] == 3) and (grid[i][j - 1] == 0) and (grid[i + 1][j]) == 0:
                grid[i][j - 1] = grid[i][j]
                grid[i][j] = 0
            j += 1
        i += 1

#Moves both the blcoks to the right
def moveRight():
    i = 0
    while i < gridHeight:
        j = gridWidth - 1
        while j >= 0:
            if (j != gridWidth - 1) and (grid[i][j] == 1 or grid[i][j] == 2 or grid[i][j] == 3) and (grid[i][j + 1] == 0) and (grid[i + 1][j]) == 0:
                grid[i][j + 1] = grid[i][j]
                grid[i][j] = 0
            j -= 1
        i += 1

#Changes the moving blocks positions
def switchBlocks():
    i = 0
    while i < gridHeight:
        j = 0
        while j < gridWidth - 1:
            if (grid[i][j] == 1 or grid[i][j] == 2 or grid[i][j] == 3):
                if (grid[i + 1][j] == 0 and grid[i][j + 1] != 0):
                    temp = grid[i][j]
                    grid[i][j] = grid[i][j+1] 
                    grid[i][j + 1] = temp
            j += 1
        i += 1

#Draws new random blocks in the top center of the screen
def drawNewBlocks():
    grid[0][2] = getRandomNum()
    grid[0][3] = getRandomNum()
    dropFast = False

#Detect if all of the blocks are not moving
def detectNewBlocks():
    i = 0
    while i < gridHeight - 1:
        j = 0
        while j < gridWidth:
            if (grid[i][j] == 1 or grid[i][j] == 2 or grid[i][j] == 3) and grid[i + 1][j] == 0:
                return False
            j += 1
        i += 1
    return True

#Detects collision between the blocks and delts them is there are more than three stacked
def detectCollision(playerScore):
    i = 0
    while i < gridHeight - 2:
        j = 0
        while j < gridWidth:
            if (grid[i][j] != 0) and (grid[i + 1][j] == grid[i][j]) and (grid[i + 2][j] == grid[i][j]):
                grid[i + 2][j] = 0
                grid[i + 1][j] = 0
                grid[i][j] = 0
                playerScore += 10
            j += 1
        i += 1
    return playerScore



#Setting up screen and time
surface = pygame.display.set_mode((X_DISPLAY, Y_DISPLAY), pygame.NOFRAME, pygame.SCALED)
surface.fill(BG_COLOR)

#MAIN GAME LOOP
pygame.display.flip()
score(playerScore)
drawNewBlocks()
homeScreen = True

while running:
    #BEFORE GAME STUFF
    while homeScreen:
        updateMenu()
        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.type != pygame.QUIT:
                   homeScreen = False 
                if event.type == pygame.QUIT:
                    running = False

    #DURING GAME STUFF
    if (alive): 
        #Detect the amount of time since the last time the block has dropped
        lastTick = currentTick
        currentTick = pygame.time.get_ticks()
        timeSinceLastTick += (currentTick - lastTick)

        #Detect and use inputs
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.type == pygame.QUIT:
                    running = False
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_LEFT:
                    moveLeft()
                if event.key == K_RIGHT:
                    moveRight()
                if event.key == K_DOWN:
                    if(dropFast):
                        dropFast = False
                    else:
                        dropFast = True
                if event.key == K_UP:
                    switchBlocks()
        
        if (dropFast and timeSinceLastTick >= 50) or (timeSinceLastTick >= 1000 / (2 * (difficulty + 1))):
            timeSinceLastTick = 0
            dropBlocks()

            if grid[0][2] != 0 or grid[0][3] != 0:
                hasDropped += 1 
            if(hasDropped >= 2):
                running = False #CHANGE TO WHAT HAPPENDS AFTER YOU LOSE

        if detectNewBlocks():
            drawNewBlocks()
            newBlocksNum += 1
            if newBlocksNum >= 10:
                newBlocksNum = 0
                difficulty += 1

        #Updating Screen
        surface.fill(BG_COLOR)
        updateUI()
        drawBlocks()
        playerScore = detectCollision(playerScore)
    
        pygame.display.flip()

    #AFTER GAME STUFF








